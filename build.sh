#!/usr/bin/env bash

set -x

cp Dockerfile-template Dockerfile
sed -i -e "s/__CI_SERVER_URL__/$CI_SERVER_URL/g" \
    -e "s/__RUNNER_NAME__/$RUNNER_NAME/g" \
    -e "s/__REGISTRATION_TOKEN__/$REGISTRATION_TOKEN/g" Dockerfile

docker login -u $CI_USERNAME -p $CI_PASSWORD registry.gitlab.com

docker build -t registry.gitlab.com/kirscht/gitlab-ci-alpine:latest .

#docker run registry.gitlab.com/kirscht/gitlab-ci-alpine:latest which gcloud

docker push registry.gitlab.com/kirscht/gitlab-ci-alpine:latest