#FROM gitlab/gitlab-runner:alpine
FROM docker:latest
RUN mkdir -p /app
WORKDIR /app
RUN apk update && \
    apk upgrade && \
    apk add --update  --no-cache bash curl ca-certificates wget python py-pip make openssl && \
    pip install webapp2 urllib3 Paste WebOb && \
    update-ca-certificates

RUN wget https://dl.google.com/dl/cloudsdk/release/google-cloud-sdk.tar.gz && \
    tar zxvf google-cloud-sdk.tar.gz && \
    ./google-cloud-sdk/install.sh --usage-reporting=false --path-update=true && \
    google-cloud-sdk/bin/gcloud --quiet components update && \
    google-cloud-sdk/bin/gcloud components install kubectl
COPY . /app
VOLUME ["/etc/gitlab-runner", "/home/gitlab-runner"]
ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["sh"]